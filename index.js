const showPasswordIcons = document.querySelectorAll('.icon-password');
showPasswordIcons.forEach(icon => {
    icon.addEventListener('click', () => {
        const input = icon.previousElementSibling;
        if (input.type === 'password') {
            input.type = 'text';
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
            icon.setAttribute('title', 'Скрыть пароль');
        } else {
            input.type = 'password';
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
            icon.setAttribute('title', 'Показать пароль');
        }
    });
});

const form = document.querySelector('.password-form');
form.addEventListener('submit', (event) => {
    event.preventDefault();
    const passwordInputs = form.querySelectorAll('input[type="password"]');
    const firstPassword = passwordInputs[0].value;
    const secondPassword = passwordInputs[1].value;
    if (firstPassword === secondPassword) {

        if(document.querySelector('p')){
            document.querySelector('p').remove();
        }
        alert('You are welcome');

    } else {
        const errorText = document.createElement('p');
        errorText.textContent = 'Необходимо ввести одинаковые значения';
        errorText.style.color = 'red';
        form.appendChild(errorText);
    }
});

